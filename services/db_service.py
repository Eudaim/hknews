"""File that handles calls to the Database"""
import sqlite3
from services import sql_service

def db_connection(): # pragma: no cover
    """Start connection with hknews DB"""
    conn = None
    try:
        conn = sqlite3.connect("database/hknews.db")
    except sqlite3.Error as except_error:
        print(except_error)
    return conn

def add_story(story): # pragma: no cover
    """Add a story to the DB"""
    conn = db_connection()
    cursor = conn.cursor()
    new_by = story['by']
    if 'descendants' in story :
        new_descendents = int(story['descendants'])
        story['descendants'] = 0
    else :
        story['descendants'] = 0
        new_descendents = 0
    new_id = story['id']
    if 'kids' in story:
        new_kids = str(story['kids'])
    else:
        new_kids = ''
        story['kids'] = ''
    new_score = int(story['score'])
    new_time = int(story['time'])
    new_title = str(story['title'])
    new_type = str(story['type'])
    if 'url' in story :
        new_url = str(story['url'])
    else:
        new_url = ''
    cursor.execute(sql_service.insert_story(), (new_by,
                                                new_descendents,
                                                int(new_id),
                                                new_kids,
                                                new_score,
                                                new_time,
                                                new_title,
                                                new_type,
                                                new_url))
    conn.commit()
    conn.close()
    return f"{story} successfully created"

def get_story_by_id(story_id): # pragma: no cover
    """Get one story by ID"""
    conn = db_connection()
    conn.row_factory = dict_factory
    cursor = conn.cursor()
    cursor.execute(sql_service.get_story_by_id(),(story_id,))
    data = cursor.fetchone()
    conn.close()
    return data

def dict_factory(cursor, row): # pragma: no cover
    """Function to change the return type of queries from tuples to dicts"""
    dict_change = {}
    for idx, col in enumerate(cursor.description):
        dict_change[col[0]] = row[idx]
    return dict_change

def get_kids(story_id): # pragma: no cover
    """Function to get the commments on a story"""
    conn = db_connection()
    cursor = conn.cursor()
    cursor.execute(sql_service.get_kids(),(story_id,))
    data = cursor.fetchone()
    conn.close()
    return data

def add_like(story_id): # pragma: no cover
    """Add +1 to score on a story"""
    conn = db_connection()
    cursor = conn.cursor()
    cursor.execute(sql_service.add_story_like(),(story_id,))
    conn.commit()
    conn.close()

def get_all_stories_by_id(): # pragma: no cover
    """Get all Story by Id from the DB"""
    conn = db_connection()
    conn.row_factory = dict_factory
    cursor = conn.cursor()
    cursor.execute(sql_service.get_all_stories_by_id())
    story_list = cursor.fetchall
    conn.close()
    return story_list

def add_dislike(story_id): # pragma: no cover
    """Add -1 to score on a story by ID"""
    conn = db_connection()
    cursor = conn.cursor()
    cursor.execute(sql_service.dislike(),(story_id,))
    conn.commit()
    conn.close()

def add_actions(user_id, story_id, action_type, time): # pragma: no cover
    """Function to add weather a user liked or disliked a post"""
    conn = db_connection()
    cursor = conn.cursor()
    cursor.execute(sql_service.store_actions(),(user_id, story_id, action_type, time))
    conn.commit()
    conn.close()

def get_actions(): # pragma: no cover
    """Function to return all action from DB"""
    conn = db_connection()
    conn.row_factory = dict_factory
    cursor = conn.cursor()
    cursor.execute(sql_service.get_actions())
    action_list = cursor.fetchall()
    cursor.close()
    return action_list

def get_action_by_id_and_email(story_id, email): # pragma: no cover
    """Function to return all actions by ID and Email"""
    conn = db_connection()
    cursor = conn.cursor()
    cursor.execute(sql_service.get_action_by_id_and_email(),(story_id, email))
    action = cursor.fetchone()
    cursor.close()
    return action

def delete_action_by_id_and_email(story_id, email): # pragma: no cover
    """Delete a user action from DB by ID and Email"""
    conn = db_connection()
    cursor = conn.cursor()
    cursor.execute(sql_service.delete_action_by_id_and_email(),(story_id, email))
    conn.commit()
    cursor.close()

def delete_stories(): # pragma: no cover
    """Remove stories from DB"""
    conn = db_connection()
    cursor = conn.cursor()
    cursor.execute(sql_service.delete_stories())
    conn.commit()
    cursor.close()
