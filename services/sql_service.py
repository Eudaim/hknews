"""File that contains all the sql query scripts"""
def insert_story():
    """insert story"""
    return """
    INSERT INTO stories (by, descendants, id, kids, score, time, title, type, url)
    VALUES (?,?,?,?,?,?,?,?,?)"""

def get_story_by_id(): # pragma: no cover
    """get story by id"""
    return """ 
    SELECT * FROM stories WHERE id = ?"""

def get_all_stories_by_id():
    """get all stories by id"""
    return """
    SELECT * FROM stories"""

def sql_insert_user():
    """sql insert user"""
    return """
    INSERT INTO user (email, liked_Stories, disliked_Stories)"""

def add_story_like():
    """increment score on a story"""
    return """
    UPDATE stories
    SET score = score + 1
    WHERE id = ?"""

def dislike():
    """decrement score on a story"""
    return """
    UPDATE stories
    SET score = score - 1
    WHERE id = ?"""

def store_actions():
    """Record a users action"""
    return """
    INSERT INTO actions(userEmail, storyId, actionType, time)
    VALUES(?,?,?,?)"""

def get_actions():
    """Retrieve action table"""
    return """
    SELECT * FROM actions ORDER BY time DESC"""

def get_action_by_id_and_email():
    """Retrieve action by ID and Email"""
    return """
    SELECT * FROM actions WHERE storyId = ? AND userEmail = ?"""

def delete_action_by_id_and_email():
    """Delete action by ID and Email"""
    return """
    DELETE FROM actions WHERE storyId = ? AND userEmail = ?"""


def get_kids():
    """Retrieve comments"""
    return """
    SELECT kids FROM stories WHERE id = ?"""

def delete_stories():
    """Removie story by DB"""
    return """
    SELECT * FROM stories LIMIT 50
    """ # pragma: no cover