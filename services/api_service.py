"""This File handles allt the API calls made to Hackernews"""
import json
import requests
from requests import Session
from services import db_service

mylist = []
def get_new_stories_id(): # pragma: no cover
    """Get top new stories from hackernews api"""
    new_stories = requests.get('https://hacker-news.firebaseio.com/v0/topstories.json?print=pretty')
    print("getting news stories...")
    return new_stories

def parse_request(list_of_stories): # pragma: no cover
    """Break down Top Stories IDs and call get each story from Hackernews"""
    stories_by_id = []
    data = list_of_stories.json()
    session = Session()
    for story_id in data:
        temp = get_story_by_id(story_id,session)
        stories_by_id.append(temp)
    return stories_by_id

def get_story_by_id(story_id,session): # pragma: no cover
    """Get a story from Hackernews by ID"""
    story = db_service.get_story_by_id(id)
    if story is not None:
        return story
    url = f'https://hacker-news.firebaseio.com/v0/item/{story_id}.json?print=pretty'
    story = session.get(url,timeout = 2)
    story = json.loads(story.content.decode('utf-8'))
    print(story)
    db_service.add_story(story)
    return story

def get_kid_by_id(story_id): # pragma: no cover
    """Get comments from a post"""
    url = f'https://hacker-news.firebaseio.com/v0/item/{story_id}.json?print=pretty'
    story = requests.get(url,timeout = 2)
    story = json.loads(story.content.decode('utf-8'))
    return story
