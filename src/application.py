"""Enpoints and logic for hknews"""
import json
import time
import secure

from os import environ as env
from urllib.parse import quote_plus, urlencode
from dotenv import find_dotenv, load_dotenv
from authlib.integrations.flask_client import OAuth
from flask_paginate import Pagination, get_page_parameter
from apscheduler.schedulers.background import BackgroundScheduler
from flask import Flask, redirect, render_template, session, url_for, request
from services import api_service, db_service, sql_service
ENV_FILE = find_dotenv()
if ENV_FILE:
    load_dotenv(ENV_FILE)

def get_stories_every_minute(): # pragma: no cover
    """Logic that handles getting stories every minute."""
    temp = api_service.get_new_stories_id()
    app.mylist = api_service.parse_request(temp)

scheduler = BackgroundScheduler()
scheduler.add_job(func=get_stories_every_minute, trigger="interval", seconds=60)
scheduler.start()

app = Flask(__name__)

app.secret_key = env.get("APP_SECRET_KEY")
app.mylist = []
app.admin_emails = ["kovacsd44@gmail.com", "ardonniss@gmail.com","chashimahiulislam@gmail.com"]
secure_headers = secure.Secure()
csp = secure.ContentSecurityPolicy()
xfo = secure.XFrameOptions().deny()
hsts_value = (
secure.StrictTransportSecurity()
.include_subdomains()
.preload()
.max_age(2592000)
)
secure_headers = secure.Secure(csp=csp, hsts=hsts_value, xfo=xfo)

oauth = OAuth(app)
oauth.register(
    "auth0",
    client_id=env.get("AUTH0_CLIENT_ID"),
    client_secret=env.get("AUTH0_CLIENT_SECRET"),
    client_kwargs={
        "scope": "openid profile email",
    },
    server_metadata_url=f'https://{env.get("AUTH0_DOMAIN")}/.well-known/openid-configuration'
)

@app.after_request
def set_secure_headers(response):
    secure_headers.framework.flask(response)
    return response

@app.route("/welcome")
def welcome():
    """Welcome page endpoint."""
    return render_template("welcome.html")

@app.route("/login")
def login():
    """Auth0 login handler."""
    return oauth.auth0.authorize_redirect( # pragma: no cover
        redirect_uri=url_for("callback", _external=True)
    )

@app.route("/callback", methods=["GET", "POST"])
def callback():
    """Auth0 callback handler."""
    token = oauth.auth0.authorize_access_token()
    session["user"] = token # pragma: no cover
    return redirect("/news")

@app.route("/logout")
def logout():
    """Auth0 logic handler for logging out."""
    session.clear()
    return redirect(
        "https://" + env.get("AUTH0_DOMAIN")
        + "/v2/logout?"
        + urlencode(
            {
                "returnTo": url_for("home", _external=True),
                "client_id": env.get("AUTH0_CLIENT_ID"),
            },
            quote_via=quote_plus,
        )
    )
@app.route("/news")
def news():
    """Handle enpoint for the news page."""
    html_template = "news.html"
    conn = db_service.db_connection()
    conn.row_factory = db_service.dict_factory
    cursor = conn.cursor()
    cursor.execute(sql_service.get_all_stories_by_id())
    app.mylist = cursor.fetchall()
    app.mylist.reverse()
    page = request.args.get(get_page_parameter(), type=int, default=1)
    per_page = 25
    offset = (page - 1) * per_page
    if page is not None:
        x_axis = (page - 1) * per_page
        y_axis = page * per_page
        story_page = app.mylist[x_axis:y_axis]
        pagination = Pagination(
            page=page,
            per_page=per_page,offset=offset,
            total=len(app.mylist),
            record_name='mylist')
    return render_template(html_template, values=story_page, pagination=pagination)
@app.route("/like/<story_id>", methods=["GET"])
def likes(story_id):
    """Handle the logic for when a user likes a story."""
    action_type = "Like"
    user=session.get('user')
    if user is None: # pragma: no cover
        return redirect("/")
    user_info = dict(user).get('userinfo')
    user_email = dict(user_info).get('email')
    db_service.delete_action_by_id_and_email(story_id, user_email)
    db_service.add_actions(user_email, story_id, action_type, time.time())
    db_service.add_like(story_id)
    return redirect("/news")

@app.route("/dislike/<user_id>", methods=["GET"])
def dislikes(user_id):
    """Handle the logic for when a user dislikes a story."""
    action_type = "Dislike"
    user=session.get('user')
    if user is None:
        return redirect("/")
    user_info = dict(user).get('userinfo')
    db_service.delete_action_by_id_and_email(user_id, dict(user_info).get('email'))
    db_service.add_actions(dict(user_info).get('email'), user_id, action_type, time.time())
    db_service.add_dislike(user_id)
    return redirect("/news")

@app.route("/admindelete/<story_id>/<email>/<action_type>", methods=["GET"])
def admin_delete(story_id,email,action_type):
    """handles the logic for when an admin deletes a like and dislike on a story."""
    db_service.delete_action_by_id_and_email(story_id, email)
    if action_type == "Dislike":
        db_service.add_like(story_id)
    else:
        db_service.add_dislike(story_id)
    return redirect("/admin")


@app.route("/story/<story_id>", methods=["GET"])
def story(story_id):
    """Handle the logic for getting a story and its comments."""
    comments = db_service.get_kids(story_id)
    string_of_comments = ""
    for i in comments:
        string_of_comments = string_of_comments + i
    string_of_comments = string_of_comments.replace('[','')
    string_of_comments = string_of_comments.replace(']','')
    list_of_comments = string_of_comments.split(',')
    comment_list = []
    for i in list_of_comments:
        post = api_service.get_kid_by_id(i)
        list.append(post)
    print(list_of_comments)
    return render_template("story.html", values=comment_list)

@app.route("/admin")
def admin():
    """Handle enpoint for the admin page."""
    user_email = "ardonniss@gmail.com"
    if user_email in app.admin_emails: 
        my_action_list = db_service.get_actions()
        story_list = []
        for i in my_action_list:
            story_list.append(db_service.get_story_by_id(i['storyId']))
        return render_template("admin.html", values=zip(my_action_list, story_list))
    return render_template("home.html")


@app.route("/profile")
def profile():
    """Handle enpoint for the profile page."""
    user=session.get('user')
    if user is None:
        return redirect("/")
    user=session.get('user')
    user_info = dict(user).get('userinfo')
    user_email = dict(user_info).get('email')
    user_name = dict(user_info).get('name')
    name_and_email = [user_name,user_email]
    return render_template("profile.html", values=name_and_email)
@app.route("/")
def home():
    """Handle enpoint for the home page."""
    return render_template(
    "home.html",
    session=session.get('user'),
    pretty=json.dumps(session.get('user'),
    indent=4))
