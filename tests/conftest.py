import pytest
import sqlite3

@pytest.fixture
def client():
    from src.application import app 
    app.config['TESTING'] = True
    return app.test_client()
