from src.application import app
import pytest 
import services.api_service
import services.db_service 
import services.sql_service
from requests import Session


def testHome(client):
    client = app.test_client()
    response = client.get('/')
    assert response.status_code == 200

def testNews(client):
    client = app.test_client()
    response = client.get('/news')
    assert response.status_code == 200

def testWelcome(client): 
    client = app.test_client()
    response = client.get('/welcome')
    assert response.status_code == 200

def testLogin(client): 
    client = app.test_client() 
    response = client.get('/login')
    assert response.status_code == 302

def testAdmin(client): 
    client = app.test_client()
    response = client.get('admin')
    assert response.status_code == 200 

def testLike(client): 
    client = app.test_client()
    response = client.get('like/123123')
    assert response.status_code == 302 

def testLogout(client): 
    client = app.test_client()
    response = client.get('/logout')
    assert response.status_code == 302

def testCallBack(): 
    response = app.test_client().get('/callback')
    assert not type(response) is None
    
def testAdminDelete(): 
    response = app.test_client().get('/admindelete/123432')
    assert not type(response) is None

def testLogin(client): 
    client = app.test_client()
    response = client.get('dislike/123123')
    assert response.status_code == 302

def testProfile(client): 
    client = app.test_client()
    response = client.get('/profile')
    assert response.status_code == 302

def testGetNewStoriesId(): 
    response = services.api_service.get_new_stories_id()
    assert not type(response) is None

def testGetKidById():
    id = 123123
    response = services.api_service.get_kid_by_id(id)
    assert not type(response) is None

def testGetStoryById(): 
    id = 24589043
    session = Session()
    response = services.api_service.get_story_by_id(id, session)
    assert not type(response) is None 

def testInsertStory(): 
    response = services.sql_service.insert_story()
    assert not type(response) is None

def testGetStoryById():
    id = 123123
    session = Session();
    response = services.sql_service.get_all_stories_by_id()
    assert not type(response) is None

def testSql_insert_user(): 
    response = services.sql_service.sql_insert_user()
    assert not type(response) is None

def testAddStoryLike():
    response = services.sql_service.add_story_like()
    assert not type(response) is None

def testDislike():
    response = services.sql_service.dislike()
    assert not type(response) is None

def testStoreActions():
    response = services.sql_service.store_actions()
    assert not type(response) is None

def testGetActions():
    response = services.sql_service.get_actions()
    assert not type(response) is None

def testGetActionByIdAndEmail():
    response = services.sql_service.get_action_by_id_and_email()
    assert not type(response) is None

def testDeleteActionByIdAndEmail():
    response = services.sql_service.delete_action_by_id_and_email()
    assert not type(response) is None

def testGetKids(): 
    response = services.sql_service.get_kids()
    assert not type(response) is None
    
def delete_stories(): 
    response = services.sql_service.delete_stories()
    assert not type(response) is None


    