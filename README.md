Ardonniss Zimero asz19
# Domain Name 
`https://www.hknew.xyz`
# Server IP address
`159.223.141.169`
# How to set up Server and Flask Application
1. Use `digitalOcean.com` to host your Ubuntu server.
2. set up ssh by following this guide `https://www.digitalocean.com/community/tutorials/ssh-essentials-working-with-ssh-servers-clients-and-keys` and use the ssh config file from the configure folder
3. Follow this guide  to learn how to serve Flask app with Nginx and Gunicorn `https://dev.to/brandonwallace/deploy-flask-the-easy-way-with-gunicorn-and-nginx-jgc`
4. Set up Auth0 by following this doc `https://auth0.com/docs/quickstart/webapp/python/interactive
deploy-flask-the-easy-way-with-gunicorn-and-nginx-jgc`
# How to run Flask Application on Server locally
1. Navigate from root into `var/www/myproject`
2. Into the myproject folder run `pipenv shell` to enter python virtual environment 
3. Run `fuser -k 5000/tcp` to kill port 5000 and makeit availible again if it isn't already
4. Run `flask run --host '0.0.0.0` to start flask app locally on application 
# How we handle updates 
We handle updates on our server by using automatic unattended updates for Ubuntu 20.04. Since update are important in order to keep are server secure we configured the unattended-upgrades package to automatically install sofware updates. The configuration for unattended updates can be found in the config folder.
![](images/Homework1.png)
# Security
One of the method we implemented a method to stop intruders from getting into our servers by setting up a firewall with ufw. By setting up a firewall we can allow specific ports to become accessible such as port 22 for ssh and port 80 for HTTP. By doing this step, the adversary would not be able to take advantage of ports we aren’t actively monitoring. In addition to that with the firewall, we are about to white list certain ip’s to ssh into the certain. Meaning that the only ones who are able to ssh into the server should be me and my partner. On the topic of ssh another method in which we secure our server is configuring our ssh config file to where the server can only be accessible with a public key. The only authorized keys that are allowed on the server is from both of our public key on our local machine, meaning that unless the adversary gets our local machine or our public keys which is highly unlikely then our server is secured. 
# Pylint Score
![](images/pylint_score.png)
# Pytest Score 
![](images/pytest_score.png)
# Mozilla Observatory Score
![](images/observe_score.png)
# Zoom Meeting Spreadsheet link
https://docs.google.com/spreadsheets/d/1wB_7KyOnm8oih0eOLpIOHrJB69PEpzYpXzmniFrYMa8/edit?usp=sharing
# [Tutorial Video](https://youtu.be/jwRrBeggvPM)
# Project Contents 
`configuration` - contains the configuration for nginx, gunicorn and ssh
<br />
`database` - contains the database for hknews website. stores user info and top stories 
from hackernews api
<br />
`images` - contains screenshot of test converage, pylint and mozilla score 
<br />
`services` - contains the all the logic and services used throughout the application 
<br />
`src` - contains the flask application 
<br />
`test` - contains all the tests for the flask appliacation 
<br />
`src/template` - contains the html templates used for the website
<br />
`coveragerc` - contains the configuration for pytest 
<br />
`wsgi.py` - flask application 
<br />
